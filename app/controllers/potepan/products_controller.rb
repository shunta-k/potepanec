class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_DISPLAY_NUMBER = 8

  def show
    @product = Spree::Product.includes([{ product_properties: :property }]).find(params[:id])
    @related_products = Spree::Product.including_images_and_price.related_products(@product).limit(RELATED_PRODUCTS_DISPLAY_NUMBER)
    if @product.has_variants?
      @selected_variant = params[:variation] ? @product.variants.find_by(id: params[:variation]) : @product.variants.first
    else
      @selected_variant = @product.master
    end
  end
end
