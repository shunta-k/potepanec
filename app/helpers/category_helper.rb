module CategoryHelper
  def product_in_taxon_counts(taxon)
    Spree::Product.in_taxon(taxon).count
  end
end
