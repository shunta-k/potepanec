module ProductHelper
  def options_texts(product)
    product.variants.includes(option_values: :option_type).map do |variant|
      values = variant.option_values.sort_by do |option_value|
        option_value.option_type.position
      end

      values.to_a.map! do |ov|
        "#{ov.option_type.presentation}: #{ov.presentation}"
      end

      [values.to_sentence({ words_connector: ", ", two_words_connector: ", " }), variant.id]
    end
  end
end
