Spree::Product.class_eval do
  scope :including_images_and_price, -> { includes(master: [:images, :default_price]) }
  scope :except_product, -> (product) { where.not(id: product.id) }
  scope :related_products, -> (product) {
    in_taxons(product.taxons).
      except_product(product).
      distinct.
      order(Arel.sql("RAND()"))
  }
end
