require 'rails_helper'

feature "Categories" do
  feature "show category page" do
    # Taxonomy
    given!(:category) { create(:taxonomy, name: "category") }

    # taxon
    given(:accessory) { create(:taxon, name: "accessory", taxonomy: category, parent: category.root) }
    given!(:bag) { create(:taxon, name: "bag", taxonomy: category, parent: accessory) }
    given!(:cup) { create(:taxon, name: "cup", taxonomy: category, parent: category.root) }

    # product
    given!(:potepan_bag) { create(:product, name: "potepan_bag", price: 20.00, taxons: [bag]) }
    given!(:ruby_bag) { create(:product, name: "ruby_bag", price: 17.99, taxons: [bag]) }
    given!(:potepan_cup) { create(:product, name: "potepan_cup", price: 12.99, taxons: [cup]) }
    given!(:ruby_cup) { create(:product, name: "ruby_cup", price: 13.99, taxons: [cup]) }

    scenario "show category page" do
      visit potepan_category_path(bag.id)
      # タイトルの表示
      expect(page).to have_title "#{bag.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title", text: bag.name
      # カテゴリに含まれる商品の表示
      expect(page).to have_selector ".productCaption", text: potepan_bag.name
      expect(page).to have_selector ".productCaption", text: potepan_bag.display_price
      expect(page).to have_selector ".productCaption", text: ruby_bag.name
      expect(page).to have_selector ".productCaption", text: ruby_bag.display_price
      # カテゴリに含まれない商品が表示されないこと
      expect(page).not_to have_selector ".productCaption", text: potepan_cup.name
      expect(page).not_to have_selector ".productCaption", text: potepan_cup.display_price
      # カテゴリ欄の表示
      expect(page).to have_selector ".panel-body", text: category.name
      expect(page).to have_selector ".panel-body", text: accessory.name
      expect(page).to have_selector ".panel-body", text: bag.name
      expect(page).to have_selector ".panel-body", text: cup.name
    end

    scenario "link to other category page" do
      visit potepan_category_path(bag.id)
      # 他カテゴリのリンクを押下
      click_on cup.name
      # cupカテゴリのページに遷移すること
      expect(page).to have_current_path(potepan_category_path(cup.id))
      # タイトルの表示
      expect(page).to have_title "#{cup.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title", text: cup.name
      # カテゴリに含まれる商品の表示
      expect(page).to have_selector ".productCaption", text: potepan_cup.name
      expect(page).to have_selector ".productCaption", text: potepan_cup.display_price
      expect(page).to have_selector ".productCaption", text: ruby_cup.name
      expect(page).to have_selector ".productCaption", text: ruby_cup.display_price
      # カテゴリに含まれない商品が表示されないこと
      expect(page).not_to have_selector ".productCaption", text: potepan_bag.name
      expect(page).not_to have_selector ".productCaption", text: potepan_bag.display_price
      expect(page).not_to have_selector ".productCaption", text: ruby_bag.name
      expect(page).not_to have_selector ".productCaption", text: ruby_bag.display_price
    end

    scenario "link to product page" do
      visit potepan_category_path(bag.id)
      # 商品のリンクを押下
      click_link potepan_bag.name
      expect(page).to have_current_path(potepan_product_path(potepan_bag.id))
      # タイトルの表示
      expect(page).to have_title "#{potepan_bag.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title", text: potepan_bag.name
      # 商品情報の表示
      expect(page).to have_selector ".mainContent .media-body", text: potepan_bag.name
      expect(page).to have_selector ".mainContent .media-body", text: potepan_bag.display_price
    end

    scenario "link to other category page to click Breadcrumbs list" do
      visit potepan_category_path(bag.id)
      # パンくずリストのaccessoryカテゴリボタンを押下
      click_link accessory.name
      expect(page).to have_current_path(potepan_category_path(accessory.id))
      # タイトルの表示
      expect(page).to have_title "#{accessory.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title", text: accessory.name
      # カテゴリに含まれる商品の表示
      expect(page).to have_selector ".productCaption", text: potepan_bag.name
      expect(page).to have_selector ".productCaption", text: potepan_bag.display_price
      expect(page).to have_selector ".productCaption", text: ruby_bag.name
      expect(page).to have_selector ".productCaption", text: ruby_bag.display_price
      # カテゴリに含まれない商品が表示されないこと
      expect(page).not_to have_selector ".productCaption", text: potepan_cup.name
      expect(page).not_to have_selector ".productCaption", text: potepan_cup.display_price
    end
  end
end
