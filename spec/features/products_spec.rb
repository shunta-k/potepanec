require 'rails_helper'

feature "Products" do
  feature "show product information" do
    # カテゴリ
    given!(:category) { create(:taxonomy, name: "category") }
    given!(:brand) { create(:taxonomy, name: "brand") }
    given!(:taxon_bag) { create(:taxon, name: "bag", taxonomy: category, parent: category.root) }
    given!(:taxon_potepan) { create(:taxon, name: "potepan", taxonomy: brand, parent: brand.root) }
    given!(:taxon_ruby) { create(:taxon, name: "ruby", taxonomy: brand, parent: brand.root) }

    # 商品
    given!(:product) { create(:product, name: "Big Bag", description: "very large bag", price: 15.99, taxons: [taxon_bag, taxon_potepan]) }

    # 関連商品
    given!(:related_product_bag) { create(:product, name: "Small Bag", price: 12.99, taxons: [taxon_bag]) }
    given!(:related_product_potepan) { create(:product, name: "Potepan Mug", price: 10.99, taxons: [taxon_potepan]) }
    given!(:related_product_ruby) { create(:product, name: "Ruby Mug", price: 8.99, taxons: [taxon_ruby]) }

    # 商品の詳細
    given!(:property1) { create(:property, name: "Manufacturer", presentation: "Manufacturer") }
    given!(:product_property1) { create(:product_property, product: product, property: property1, value: "Potepan") }
    given!(:property2) { create(:property, name: "Model", presentation: "Model") }
    given!(:product_property2) { create(:product_property, product: product, property: property2, value: "AA110") }

    # 商品のバリエーション
    given!(:option_type) { create(:option_type, name: "Color", presentation: "Color") }
    given!(:option_value1) { create(:option_value, option_type: option_type, name: "Red", presentation: "Red") }
    given!(:option_value2) { create(:option_value, option_type: option_type, name: "Blue", presentation: "Blue") }
    given!(:variant1) { create(:variant, product: product, option_values: [option_value1]) }
    given!(:variant2) { create(:variant, product: product, option_values: [option_value2]) }

    scenario "show product information" do
      visit potepan_product_path(product.id)
      # タイトルの表示
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      # 商品の基本情報の表示
      expect(page).to have_selector ".media-body h2", text: product.name
      expect(page).to have_selector ".media-body p", text: product.description
      expect(page).to have_selector ".media-body h3", text: product.display_price
      # 商品の詳細情報の表示
      expect(page).to have_selector ".list-unstyled", text: product_property1.property.presentation
      expect(page).to have_selector ".list-unstyled", text: product_property1.value
      expect(page).to have_selector ".list-unstyled", text: product_property2.property.presentation
      expect(page).to have_selector ".list-unstyled", text: product_property2.value
      # 商品のバリエーションの表示
      expect(page).to have_selector ".quick-drop", text: "Color: Red"
      expect(page).to have_selector ".quick-drop", text: "Color: Blue"
      # 関連商品の表示
      expect(page).to have_selector ".productsContent", text: related_product_bag.name
      expect(page).to have_selector ".productsContent", text: related_product_bag.display_price
      expect(page).to have_selector ".productsContent", text: related_product_potepan.name
      expect(page).to have_selector ".productsContent", text: related_product_potepan.display_price
      # 同カテゴリではない商品が表示されないこと
      expect(page).not_to have_selector ".productsContent", text: related_product_ruby.name
      expect(page).not_to have_selector ".productsContent", text: related_product_ruby.display_price
    end

    scenario "link to related product page" do
      visit potepan_product_path(product.id)
      # 商品のリンクを押下
      click_link related_product_bag.name
      expect(page).to have_current_path(potepan_product_path(related_product_bag.id))
      # タイトルの表示
      expect(page).to have_title "#{related_product_bag.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title", text: related_product_bag.name
      # 商品情報の表示
      expect(page).to have_selector ".mainContent .media-body", text: related_product_bag.name
      expect(page).to have_selector ".mainContent .media-body", text: related_product_bag.display_price
    end
  end
end
