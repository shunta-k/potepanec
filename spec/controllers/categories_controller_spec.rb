require 'rails_helper'

describe Potepan::CategoriesController do
  describe 'GET #show' do
    let(:category) { create(:taxonomy, name: "category") }
    let(:brand) { create(:taxonomy, name: "brand") }
    let(:taxon) { create(:taxon, name: "bag", taxonomy: category, parent: category.root) }
    # taxonに含まれる商品
    let(:potepan_bag) { create(:product, name: "potepan_bag", taxons: [taxon]) }
    let(:ruby_bag) { create(:product, name: "ruby_bag", taxons: [taxon]) }
    # taxonに含まれない商品
    let(:potepan_cup) { create(:product, name: "potepan_cup",) }

    before do
      get :show, params: { id: taxon.id }
    end

    # @taxonomiesに全てのtaxonomyが含まれていること
    it "assigns all taxonomies to @taxonomies" do
      expect(assigns(:taxonomies)).to match_array [category, brand]
    end

    # @taxonに要求されたカテゴリが割り当てられること
    it "assigns the requested taxon to @taxon" do
      expect(assigns(:taxon)).to eq taxon
    end

    # @productsに選択されたカテゴリに含まれる商品が割り当てられていること
    it "assigns the requested products to @products" do
      expect(assigns(:products)).to match_array [potepan_bag, ruby_bag]
    end

    # @productsに選択されたカテゴリに含まれない商品が割り当てられていないこと
    it "not assigns the non-requested products to @products" do
      expect(assigns(:products)).not_to include potepan_cup
    end

    # :showテンプレートを表示すること
    it "renders the :show template" do
      expect(response).to render_template :show
    end

    # HTTPリクエストが成功すること
    it "responds 200 suceess" do
      expect(response.status).to eq 200
    end
  end
end
