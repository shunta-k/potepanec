require 'rails_helper'

describe Potepan::ProductsController do
  describe 'GET #show' do
    let(:category) { create(:taxonomy, name: "category") }
    let(:brand) { create(:taxonomy, name: "brand") }
    let(:taxon_bag) { create(:taxon, name: "bag", taxonomy: category, parent: category.root) }
    let(:taxon_potepan) { create(:taxon, name: "potepan", taxonomy: brand, parent: brand.root) }
    let(:taxon_ruby) { create(:taxon, name: "ruby", taxonomy: brand, parent: brand.root) }
    let(:product) { create(:product, taxons: [taxon_bag, taxon_potepan]) }
    let(:related_product_bag) { create(:product, taxons: [taxon_bag]) }
    let(:related_product_potepan) { create(:product, taxons: [taxon_potepan]) }
    let(:related_product_ruby) { create(:product, taxons: [taxon_ruby]) }

    before do
      get :show, params: { id: product.id }
    end

    # @productに要求された商品が割り当てられること
    it "assigns the requested product to @product" do
      expect(assigns(:product)).to eq product
    end

    # @related_productsに@productと同じカテゴリの商品が含まれていること
    it "assigns the requested products to @related_products" do
      expect(assigns(:related_products)).to match_array [related_product_bag, related_product_potepan]
    end

    # @related_productsに@productと異なるカテゴリの商品が含まれていないこと
    it "not assigns requested products to @related_products" do
      expect(assigns(:related_products)).not_to include related_product_ruby
    end

    # @related_productsに9つ以上のproductが含まれていないこと
    it "not assigns 10 and over requested products to @related_products" do
      create_list(:product, 9, taxons: [taxon_bag])
      expect(assigns(:related_products).size).to eq 8
    end

    # :showテンプレートを表示すること
    it "renders the :show template" do
      expect(response).to render_template :show
    end

    # HTTPリクエストが成功すること
    it "responds 200 suceess" do
      expect(response.status).to eq 200
    end
  end
end
